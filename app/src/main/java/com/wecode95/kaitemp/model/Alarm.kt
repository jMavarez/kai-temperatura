package com.wecode95.kaitemp.model

import android.arch.persistence.room.*
import com.wecode95.kaitemp.util.SmsUtil
import java.util.*

@Entity(
    tableName = "alarm",
    indices = [(Index("id")), (Index("device_phone"))],
    foreignKeys = [(ForeignKey(entity = Device::class,
        parentColumns = arrayOf("phone"),
        childColumns = arrayOf("device_phone"),
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE))]
)

data class Alarm(
    @ColumnInfo(name = "device_phone") var devicePhone: String,
    @ColumnInfo(name = "tag") var tag: Int,
    @ColumnInfo(name = "type") var type: Int,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "value") var value: String = "0",
    @ColumnInfo(name = "minTemperature") var minTemperature: Int = -55,
    @ColumnInfo(name = "maxTemperature") var maxTemperature: Int = 99,
    @ColumnInfo(name = "last_checked") var last_checked: Date,
    @PrimaryKey @ColumnInfo(name = "id") var id: String = "$devicePhone$tag"
) {

  companion object {
    fun getIntRange(r: String): Int {
      if (r.isEmpty()) return 0
      return if (r.contains("+") || !r.contains("-")) r.replace("+", "").toInt()
      else r.replace("-", "").toInt() * (-1)
    }

    fun getStringRange(r: Int): String {
      return if (r >= 0) {
        if (r < 10) "+0$r"
        else "+$r"
      } else {
        if (r > -10) "-0${r * -1}"
        else r.toString()
      }
    }
  }

  fun type(): TYPE {
    return if (type == 1) TYPE.ALARM else TYPE.VOLTAGE
  }

  override fun equals(other: Any?): Boolean {
    return super.equals(other)
  }

  override fun hashCode(): Int {
    return super.hashCode()
  }

  override fun toString(): String {
    return """
            device_phone:   $devicePhone,
            tag:            $tag,
            type:           $type,
            name:           $name,
            value:          $value,
            minTemperature: $minTemperature,
            maxTemperature: $maxTemperature,
            last_checked:   ${SmsUtil.getStringDate(last_checked)}
            id:             $id,
        """
  }

  enum class TYPE {
    ALARM, VOLTAGE
  }
}
