package com.wecode95.kaitemp

import dagger.Component

@Component(
    modules = []
)

interface SmsServiceComponent {
  fun inject(into: SmsService)
}