package com.wecode95.kaitemp.util

infix fun <T> Boolean.then(value: T?) = TernaryExpression(this, value)

class TernaryExpression<out T>(private val flag: Boolean, private val truly: T?) {
  infix fun <T> or(falsy: T?) = if (flag) truly else falsy
}
