package com.wecode95.kaitemp.data

import android.arch.persistence.room.*
import com.wecode95.kaitemp.model.AuthorizedUser

@Dao
abstract class AuthorizedUserDao {
  @Query("SELECT * FROM authorized_phones WHERE device_phone = :phone")
  abstract fun authorizedPhones(phone: String): List<AuthorizedUser>

  @Query("SELECT * FROM authorized_phones WHERE device_phone = :devicePhone AND phone = :phone")
  abstract fun authorizedPhone(devicePhone: String, phone: String): AuthorizedUser?

  @Query("DELETE FROM authorized_phones WHERE device_phone = :devicePhone")
  abstract fun deleteByPhone(devicePhone: String)

  fun createOrUpdate(authorizedPhone: AuthorizedUser) {
    insertOrUpdate(authorizedPhone)
  }

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  abstract fun insertOrUpdate(vararg authorizedPhones: AuthorizedUser)

  @Delete
  abstract fun delete(vararg authorizedPhone: AuthorizedUser)

  @Update
  abstract fun update(authorizedPhone: AuthorizedUser)
}
