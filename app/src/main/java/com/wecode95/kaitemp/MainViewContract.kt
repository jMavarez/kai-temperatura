package com.wecode95.kaitemp

import com.wecode95.kaitemp.model.Device

interface MainViewContract {
  fun showDevices(devices: List<Device>)
  fun showNoDevices()
  fun showError(error: String?)
}