package com.wecode95.kaitemp

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v13.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.AuthorizedUser
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.DeviceViewModel
import com.wecode95.kaitemp.ui.adapter.DevicePagerAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainViewContract {

  @Inject
  lateinit var db: AppDatabase
  @Inject
  lateinit var pref: SharedPreferences

  private lateinit var presenter: MainPresenter
  private val devicePagerAdapter = DevicePagerAdapter(supportFragmentManager)
  private var updateReceiver: BroadcastReceiver? = null

  private lateinit var deviceViewModel: DeviceViewModel

  private lateinit var commandManager: CommandManager
  private lateinit var commandListenerImpl: CommandListenerImpl

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    KaiApplication.injector.inject(this@MainActivity)

    setSupportActionBar(toolbar)
    supportActionBar?.title = "KAI TEMPERATURA"

    deviceViewModel = ViewModelProviders.of(this@MainActivity).get(DeviceViewModel::class.java)
    commandListenerImpl = CommandListenerImpl(this@MainActivity, db)
    commandManager = CommandManager(this@MainActivity, commandListenerImpl)

    presenter = MainPresenter(this, deviceViewModel, AndroidSchedulers.mainThread())

    refresh.setOnClickListener { refresh() }
    configFab.setOnClickListener { config() }
    addNewDevice.setOnClickListener { addNewDevice() }

    tabs.setupWithViewPager(viewPager)
    viewPager.adapter = devicePagerAdapter

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS), 1)
    }
  }

  private fun getCurrentDevice(): Device {
    Timber.d("Current Item ${viewPager.currentItem}")
    Timber.d("Current Device ${devicePagerAdapter.getDevice(viewPager.currentItem)}")
    return devicePagerAdapter.getDevice(viewPager.currentItem)
  }

  private fun refresh() {
    val device = getCurrentDevice()
    commandManager.executeCommand(device.phone, DeviceCommand.REQUEST_SIGNALS_STATUS, DeviceCommand.REQUEST_SIGNALS_STATUS)
  }

  private fun config() {
    val device = getCurrentDevice()

    BottomSheetBuilder(this@MainActivity)
        .setMode(BottomSheetBuilder.MODE_LIST)
        .setMenu(R.menu.menu_device)
        .setItemClickListener {
          when (it.itemId) {
            R.id.changeName -> {
              MaterialDialog.Builder(this@MainActivity)
                  .title(R.string.change_name)
                  .content(R.string.phone)
                  .inputType(InputType.TYPE_CLASS_TEXT)
                  .input("KAITEMPERATURA", device.name, { _, _ -> })
                  .inputRange(10, 10)
                  .onPositive({ dialog, _ ->
                    val newKaiName = dialog.inputEditText!!.text.toString()
                    commandManager.executeCommand(device.phone,
                        DeviceCommand.changeDeviceName(newKaiName),
                        DeviceCommand.REQUEST_RTU_NAME_CHANGE)
                  })
                  .show()
            }
            R.id.changePass -> {
              MaterialDialog.Builder(this@MainActivity)
                  .title(R.string.change_pass)
                  .content(R.string.phone)
                  .inputType(InputType.TYPE_CLASS_TEXT)
                  .input("", device.password, { _, _ -> })
                  .inputRange(5, 5)
                  .onPositive({ dialog, _ ->
                    val newPassword = dialog.inputEditText!!.text.toString()
                    commandManager.executeCommand(device.phone,
                        DeviceCommand.changePassword(newPassword),
                        DeviceCommand.REQUEST_PASSWORD_CHANGE)
                  })
                  .show()
            }
            R.id.addNumber -> {
              Observable.fromCallable { deviceViewModel.getAuthorizedPhones(device.phone) }
                  .subscribeOn(Schedulers.computation())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe { authorizedPhones ->
                    if (authorizedPhones.size < 5) {
                      MaterialDialog.Builder(this@MainActivity)
                          .title(R.string.add_new_phone)
                          .content(R.string.phone)
                          .inputType(InputType.TYPE_CLASS_TEXT)
                          .input("04121234567", "", { _, _ -> })
                          .inputRange(11, 11)
                          .onPositive({ dialog, _ ->
                            val newPhone = dialog.inputEditText!!.text.toString()
                            commandManager.executeCommand(device.phone,
                                DeviceCommand.addPhone(newPhone),
                                DeviceCommand.REQUEST_PHONE_CHANGE)
                          })
                          .show()
                    } else {
                      Snackbar.make(mainView, "Numero maximo de telefonos alcanzado", Snackbar.LENGTH_LONG).show()
                    }
                  }
            }
            R.id.deleteNumber -> {
              Observable.fromCallable { deviceViewModel.getAuthorizedPhones(device.phone) }
                  .subscribeOn(Schedulers.computation())
                  .map { convertListToString(it) }
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe {
                    MaterialDialog.Builder(this@MainActivity)
                        .title(R.string.delete_number)
                        .autoDismiss(false)
                        .items(it)
                        .itemsCallback { _, _, position, _ ->
                          MaterialDialog.Builder(this@MainActivity)
                              .title("Confirmar")
                              .content("Esta seguro de elimar el numero '${it[position]}'")
                              .positiveText("OK")
                              .onPositive({ _, _ ->
                                commandManager.executeCommand(device.phone,
                                    DeviceCommand.removePhone(it[position]),
                                    DeviceCommand.REQUEST_PHONE_CHANGE)
                              })
                              .show()
                        }
                        .show()
                  }
            }
            R.id.toggleDailyStatus -> {
              commandManager.executeCommand(device.phone, DeviceCommand.REQUEST_AUTO_MONITOR, DeviceCommand.toggleDailyNotification())
            }
            R.id.deleteKai -> {
              val phone = pref.getString("phone", "")
              commandManager.executeCommand(device.phone, DeviceCommand.removePhone(phone), DeviceCommand.REQUEST_PHONE_SELF_DESTRUCT)
            }
          }
        }
        .setIconTintColor(ContextCompat.getColor(this@MainActivity, R.color.colorSecondaryText))
        .createDialog()
        .show()
  }

  private fun addNewDevice() {
    val phone = pref.getString("phone", null)
    Timber.d(phone)
    phone?.let {
      MaterialDialog.Builder(this@MainActivity)
          .title(R.string.add_kai)
          .content(R.string.phone)
          .inputType(InputType.TYPE_CLASS_PHONE)
          .input("04121234567", "", { _, _ -> })
          .inputRange(11, 11)
          .onPositive({ dialog, _ ->
            val devicePhone = dialog.inputEditText!!.text.toString()
            commandManager.executeCommand(devicePhone,
                DeviceCommand.addPhoneWithPass(phone, DeviceCommand.DEFAULT_PASS),
                DeviceCommand.REQUEST_PHONE_CHANGE)
          })
          .show()
    } ?: Toast.makeText(this, getString(R.string.must_add_phone), Toast.LENGTH_SHORT).show()
  }

  private fun convertListToString(list: List<AuthorizedUser>): List<String> {
    val newList = arrayListOf<String>()
    list.forEach { if (!it.phone.contains("*")) newList.add(it.phone) }
    return newList
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.menu_main, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    val id = item.itemId

    when (id) {
    // TODO: Remove in production
      R.id.action_settings -> {
      }
      R.id.editPhone -> {
        val phone = pref.getString("phone", "")

        MaterialDialog.Builder(this@MainActivity)
            .title(R.string.edit_phone)
            .content(getString(R.string.phone))
            .inputType(InputType.TYPE_CLASS_PHONE)
            .input("04121234567", phone, { _, _ -> })
            .inputRange(11, 11)
            .onPositive({ dialog, _ ->
              val localPhone = dialog.inputEditText!!.text.toString()
              pref.edit().putString("phone", localPhone).apply()
            })
            .show()
      }
      R.id.addDevice -> {
        addNewDevice()
      }
      R.id.about -> {
      }
    }

    return super.onOptionsItemSelected(item)
  }

  @SuppressLint("RestrictedApi")
  override fun showDevices(devices: List<Device>) {
    devicePagerAdapter.bind(devices)

    emptyView.visibility = View.GONE
    tabs.visibility = View.VISIBLE
    refresh.visibility = View.VISIBLE
    configFab.visibility = View.VISIBLE
  }

  @SuppressLint("RestrictedApi")
  override fun showNoDevices() {
    emptyView.visibility = View.VISIBLE
    tabs.visibility = View.GONE
    refresh.visibility = View.GONE
    configFab.visibility = View.GONE
  }

  override fun showError(error: String?) {
    showNoDevices()
  }

  override fun onStart() {
    super.onStart()
    presenter.getDevices()

    if (updateReceiver == null) {
      updateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
          val shouldUpdate = intent.extras.getBoolean("update")

          if (shouldUpdate) {
            presenter.getDevices()
          }
        }
      }
    }

    registerReceiver(updateReceiver, IntentFilter("notify_update"))
  }

  override fun onStop() {
    super.onStop()
    updateReceiver?.let { unregisterReceiver(it) }
    commandListenerImpl.onCommandOnPause()
    presenter.dispose()
  }
}
