package com.wecode95.kaitemp.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder
import com.wecode95.kaitemp.CommandListenerImpl
import com.wecode95.kaitemp.CommandManager
import com.wecode95.kaitemp.DeviceCommand
import com.wecode95.kaitemp.R
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.ui.adapter.DevicesAdapter
import com.wecode95.kaitemp.ui.adapter.DevicesAdapter.SimpleAlarmClick
import com.wecode95.kaitemp.util.plus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.edit_ranges_alarm.view.*
import kotlinx.android.synthetic.main.fragment_device.*
import timber.log.Timber

class KaiFragment : Fragment() {

  private lateinit var alarmViewModel: AlarmViewModel
  private lateinit var deviceAdapter: DevicesAdapter
  private lateinit var layoutManager: LinearLayoutManager
  private lateinit var devicePhone: String
  private var disposables = CompositeDisposable()
  private lateinit var commandManager: CommandManager

  companion object {
    val DEVICE_ID: String = "DEVICE_ID"

    fun newInstance(devicePhone: String): KaiFragment {
      val kaiFragment = KaiFragment()
      val args = Bundle()
      args.putString(DEVICE_ID, devicePhone)
      kaiFragment.arguments = args
      return kaiFragment
    }
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view: View = inflater.inflate(R.layout.fragment_device, container, false)

    commandManager = CommandManager(context!!, CommandListenerImpl(context!!, AppDatabase.createPersistentDatabase(context!!)))

    alarmViewModel = ViewModelProviders.of(this@KaiFragment).get(AlarmViewModel::class.java)
    deviceAdapter = DevicesAdapter(context!!, object : SimpleAlarmClick {
      override fun onAlarmClick(alarm: Alarm, position: Int) {
        val bottomSheet = BottomSheetBuilder(activity)
            .setMode(BottomSheetBuilder.MODE_LIST)
            .setIconTintColor(ContextCompat.getColor(context!!, R.color.colorSecondaryText))
            .setItemClickListener {
              when (it.itemId) {
                R.id.editName -> {
                  MaterialDialog.Builder(context!!)
                      .title(R.string.change_name)
                      .content(R.string.phone)
                      .inputType(InputType.TYPE_CLASS_TEXT)
                      .input("KAITEMPERATURA", alarm.name, { _, _ -> })
                      .inputRange(1, 10)
                      .onPositive({ dialog, _ ->
                        val newAlarmName = dialog.inputEditText!!.text.toString()
                        commandManager.executeCommand(devicePhone,
                            DeviceCommand.changeAlarmName(alarm.tag, newAlarmName),
                            DeviceCommand.REQUEST_ALARM_NAME_CHANGE)
                      })
                      .show()
                }
                R.id.editRange -> {
                  editRangesView(alarm)
                }
              }
            }

        if (alarm.type == 1) {
          bottomSheet.addItem(R.id.editName, getString(R.string.change_name_alarm), R.drawable.edit)
              .addItem(R.id.editRange, getString(R.string.change_range_alarm), R.drawable.edit)
        } else {
          bottomSheet.addItem(R.id.editName, getString(R.string.change_name_alarm), R.drawable.edit)
          bottomSheet.createDialog().show()
        }
      }
    })

    layoutManager = LinearLayoutManager(this@KaiFragment.context)

    recyclerAlarms.layoutManager = layoutManager
    recyclerAlarms.adapter = deviceAdapter
    recyclerAlarms.itemAnimator = DefaultItemAnimator().apply { supportsChangeAnimations = false }

    return view
  }

  fun editRangesView(alarm: Alarm) {
    val view = this.layoutInflater.inflate(R.layout.edit_ranges_alarm, null)

    view.maxRange.setText(alarm.maxTemperature.toString())
    view.minRange.setText(alarm.minTemperature.toString())

    MaterialDialog.Builder(context!!)
        .title(getString(R.string.change_ranges))
        .customView(view, false)
        .positiveText("ok")
        .autoDismiss(false)
        .onPositive { dialog, _ ->
          val max = Alarm.getStringRange(Alarm.getIntRange(view.maxRange.text.toString()))
          val min = Alarm.getStringRange(Alarm.getIntRange(view.minRange.text.toString()))

          if (areValidRanges(view.maxRange.text.toString(), view.minRange.text.toString())) {
            Timber.d(DeviceCommand.changeAlarmRange(alarm.tag, min, max))
            commandManager.executeCommand(devicePhone,
                DeviceCommand.changeAlarmRange(alarm.tag, view.minRange.text.toString(), view.maxRange.text.toString()),
                DeviceCommand.REQUEST_ALARM_RANGE_CHANGE)
            dialog.dismiss()
          }
        }
        .show()
  }

  private fun areValidRanges(max: String, min: String): Boolean {
    if (max.length > 3) {
      Toast.makeText(context, "Maximo 3 caracteres en valor Minimo de temperatura", Toast.LENGTH_SHORT).show()
      return false
    } else if (min.length > 3) {
      Toast.makeText(context, "Maximo 3 caracteres en valor Maximo de temperatura", Toast.LENGTH_SHORT).show()
      return false
    } else if (max.isEmpty() || min.isEmpty()) {
      Toast.makeText(context, "Ingrese valor de temperatura", Toast.LENGTH_SHORT).show()
      return false
    } else if (max.isNotEmpty() && min.isNotEmpty()) {
      val maxInt = Alarm.getIntRange(max)
      val minInt = Alarm.getIntRange(min)
      return when {
        minInt >= maxInt -> {
          Toast.makeText(context, "Valor Minimo no debe ser mayor al Maximo", Toast.LENGTH_SHORT).show()
          false
        }
        maxInt > 99 -> {
          Toast.makeText(context, "Valor Maximo no debe ser mayor a 99", Toast.LENGTH_SHORT).show()
          false
        }
        minInt < -50 -> {
          Toast.makeText(context, "Valor Maximo no debe ser menor a -50", Toast.LENGTH_SHORT).show()
          false
        }
        else -> true
      }
    } else {
      return true
    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    devicePhone = arguments!!.getString(DEVICE_ID)

    disposables += alarmViewModel.getAlarmsByDevice(devicePhone)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { alarms ->
          deviceAdapter.bind(alarms)
          Timber.d("Device $devicePhone alarms: ${alarms.size}")
        }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    disposables.clear()
  }
}
