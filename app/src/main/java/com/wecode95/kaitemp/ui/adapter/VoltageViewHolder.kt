package com.wecode95.kaitemp.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.wecode95.kaitemp.R
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.util.SmsUtil
import kotlinx.android.synthetic.main.item_voltage.view.*

class VoltageViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

  fun bind(alarm: Alarm) {
    view.alarmDrawable.setImageResource(R.drawable.ic_high_voltage)
    view.alarmName.text = alarm.name
    view.alarmStatus.text = alarm.value
    view.alarmLastChecked.text = SmsUtil.getStringDate(alarm.last_checked)
  }

}

