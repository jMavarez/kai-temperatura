package com.wecode95.kaitemp.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.KaiFragment

class DevicePagerAdapter constructor(private val fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
  private var devices: List<Device> = emptyList()
  private var fragmentPool: ArrayList<Fragment> = ArrayList()

  override fun getItem(position: Int): Fragment {
    return KaiFragment.newInstance(devicePhone = devices[position].phone)
  }

  override fun getCount(): Int {
    return devices.size
  }

  override fun getPageTitle(position: Int): CharSequence {
    return devices[position].name
  }

  override fun getItemPosition(`object`: Any): Int {
    val fragment: Fragment = `object` as Fragment
    fragmentPool = ArrayList(this.fragmentManager.fragments)

    return if (fragmentPool.contains(fragment)) {
      PagerAdapter.POSITION_NONE
    } else {
      PagerAdapter.POSITION_UNCHANGED
    }
  }

  fun getDevice(position: Int): Device {
    return devices[position]
  }

  fun bind(devices: List<Device>) {
    if (devices.isNotEmpty()) {
      this.devices = emptyList()
      this.devices = devices;
      this.notifyDataSetChanged()
    }
  }
}
