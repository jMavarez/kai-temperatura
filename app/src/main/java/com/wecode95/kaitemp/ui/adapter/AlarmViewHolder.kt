package com.wecode95.kaitemp.ui.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import com.wecode95.kaitemp.R
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.util.SmsUtil
import kotlinx.android.synthetic.main.item_alarm.view.*

internal class AlarmViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

  fun bind(alarm: Alarm) {
    view.alarmDrawable.setImageResource(R.drawable.ic_thermometer)
    view.alarmName.text = alarm.name
    view.alarmLastChecked.text = SmsUtil.getStringDate(alarm.last_checked)
    view.alarmTemperature.text = "${alarm.value}º"
    view.alarmTemperatureProgress.maxProgress = 99f

    if (alarm.value.trim().isNotEmpty()) {
      view.alarmTemperatureProgress.progress = alarm.value.toFloat()
    }

    view.alarmTemperatureProgress.progressColor = Color.parseColor("#64DD17")
  }

}
