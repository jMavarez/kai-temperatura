package com.wecode95.kaitemp

import com.wecode95.kaitemp.ui.DeviceViewModel
import com.wecode95.kaitemp.util.plus
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainPresenter(private val view: MainViewContract, private val db: DeviceViewModel, private val observeOn: Scheduler) {

  private var disposable = CompositeDisposable()

  fun getDevices() {
    disposable += db.getDevices()
        .subscribeOn(Schedulers.io())
        .observeOn(observeOn)
        .subscribe({ devices ->
          if (devices.isNotEmpty()) {
            view.showDevices(devices)
          } else {
            view.showNoDevices()
          }
        }, { error ->
          view.showError(error.message)
        }
        )
  }

  fun dispose() {
    disposable.clear()
  }
}