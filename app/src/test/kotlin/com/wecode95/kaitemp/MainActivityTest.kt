package com.wecode95.kaitemp

import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.DeviceViewModel
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.Mockito.`when` as _when

@Suppress("IllegalIdentifier")
class MainActivityTest {

  private lateinit var presenter: MainPresenter

  private var view: MainViewContract = mock(MainViewContract::class.java)
  private var deviceDao: DeviceViewModel = mock(DeviceViewModel::class.java)

  private val DEVICES: List<Device> = arrayListOf(Device(), Device(), Device())

  @Before
  fun setUp() {
    presenter = MainPresenter(view, deviceDao, Schedulers.trampoline())
  }

  @Test
  fun shouldShowDevices() {
    _when(deviceDao.getDevices()).thenReturn(Flowable.just(DEVICES))

    presenter.getDevices()

    verify(view).showDevices(DEVICES)
  }

  @Test
  fun shouldShowNoDevices() {
    _when(deviceDao.getDevices()).thenReturn(Flowable.just(emptyList()))

    presenter.getDevices()

    verify(view).showNoDevices()
  }

}